class Category {
  Category({
    this.title = '',
    this.imagePath = '',
    this.lessonCount = 0,
    this.money = 0,
    this.rating = 0.0,
  });

  String title;
  int lessonCount;
  int money;
  double rating;
  String imagePath;

  static List<Category> categoryList = <Category>[
    Category(
      imagePath: 'assets/design_course/interFace1.png',
      title: 'Vai allo SHOP!',
      lessonCount: 24,
      money: 25,
      rating: 4.3,
    )
  ];

  static List<Category> infoList = <Category>[
    Category(
      imagePath: 'assets/design_course/carrello.png',
      title: 'Scegli i prodotti che vuoi acquistare',
      lessonCount: 28,
      money: 208,
      rating: 4.9,
    ),
    Category(
      imagePath: 'assets/design_course/paga.png',
      title: 'Paga direttamente online',
      lessonCount: 28,
      money: 208,
      rating: 4.9,
    ),
    Category(
      imagePath: 'assets/design_course/casa.png',
      title: 'Ritira la tua spesa o ricevila a casa',
      lessonCount: 28,
      money: 208,
      rating: 4.9,
    ),
  ];
}
